<?php
    include 'connect.php';

    session_start();
    $gender_list = ["Nữ", "Nam"];
    $faculty_list = array(""=>"trống", "MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học vật liệu");

    $userName = $_SESSION['userName'];
    $gender = $gender_list[$_SESSION['gender']-1];
    $faculty = $faculty_list[$_SESSION['faculty']];
    $birthday = $_SESSION['birthday'];
    $address = $_SESSION['address'];
    if (isset($_SESSION['img'])) {
        $img = $_SESSION['img'];
    }
    else {
        $img = '';
    }

    if (isset($_POST["submit"])) {

        $parts = explode('/', $_SESSION['birthday']);
        $date  = "$parts[2]-$parts[1]-$parts[0]";

        if (isset($_SESSION['img'])) {
            $img = $_SESSION['img'];
        }
        else {
            $img = null;
        }

        $stmt = $conn->prepare("INSERT INTO student (name, gender, faculty, birthday, address, avartar) 
                                VALUES (:name, :gender, :faculty, :birthday, :address, :avartar)");
        $data = [":name" => $_SESSION["userName"],
                ":gender" => $_SESSION["gender"]-1,
                ":faculty" => $_SESSION["faculty"],
                ":birthday" => $date,
                ":address" => $_SESSION["address"],
                ":avartar" => $img];

        $stmt->execute($data);
        header('Location: '.'complete_regist.php');  
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/confirm.css">
</head>
<body>
    <div class="wrapper">
        <div class="input-box name-box">
            <label for="" class="label-input">
                Họ và tên
            </label>
            <label for=""><?php echo $userName; ?></label>
        </div>
        <div class="input-box name-box">
            <label for="" class="label-input">
                Giới tính
            </label>
            <label for=""><?php echo $gender; ?></label>
        </div>
        <div class="input-box name-box">
            <label for="" class="label-input">
                Phân khoa
            </label>
            <label for=""><?php echo $faculty; ?></label>
        </div>
        <div class="input-box name-box">
            <label for="" class="label-input">
                Ngày sinh
            </label>
            <label for=""><?php echo $birthday; ?></label>
        </div>
        <div class="input-box name-box">
            <label for="" class="label-input">
                Địa chỉ
            </label>
            <label for=""><?php echo $address; ?></label>
        </div>
        <div class="input-box image-box">
            <label for="" class="label-input">
                Hình ảnh
            </label>
            <img src="<?php echo $img; ?>" alt="" width="80px" height="50px" class="image">
        </div>
        <form action="confirm.php" method="post">
            <div class="button-box">
                <button class="button-submit" type="submit" name="submit">Xác nhận</button>
            </div>
        </form>
    </div>
</body>
</html>